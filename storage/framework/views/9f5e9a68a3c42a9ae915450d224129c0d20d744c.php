<?php $__env->startSection('content'); ?>
<!-- END HEADER -->
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Sale</h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE CONTENT BODY -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE BREADCRUMBS -->
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="<?php echo e(route('home')); ?>">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="#">Sales</a>
                    </li>
                </ul>
                <!-- END PAGE BREADCRUMBS -->
                <?php if(count($sales_bill) > 0): ?>
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="page-content-inner">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-gift"></i>
                                    </div>
                                    <!-- <div class="tools">
                                        <a href="javascript:;" class="collapse"> </a>
                                        <a href="javascript:;" class="reload"> </a>
                                    </div> -->
                                </div>
                                <div class="portlet-body">
                                    <!-- BEGIN FORM-->
                                    <form action="<?php echo e(route('sales.temp.details')); ?>" method="POST" class="form-horizontal">
                                        <?php echo e(csrf_field()); ?>

                                        <div class="form-body">
                                            <h3 class="form-section">Info</h3>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Date</label>
                                                        <div class="col-md-9">
                                                            <input type="date" class="form-control" id="cudate" name="cudate" required value="<?php echo e($sales_bill[0]->bill_date); ?>" placeholder="Dadte">
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Bill number automatic -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Bill</label>
                                                        <div class="col-md-9">
                                                            <input type="text" class="form-control" id="billno" name="billno" readonly value="<?php echo e($sales_bill[0]->bill_no); ?>" placeholder="Automatic">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                                $sales_bill_temps_va = DB::table('customers')
                                                            ->select('*')
                                                            ->where('id', $sales_bill[0]->customer_id)
                                                            ->get();
                                            
                                                $customer = DB::table('customers')
                                                            ->select('*')
                                                            ->where('id', $sales_bill[0]->customer_id)
                                                            ->get();
                                            
                                                $purchaser = DB::table('customers')
                                                            ->select('*')
                                                            ->where('id', $sales_bill[0]->purchaser_id)
                                                            ->get();
                                            ?>
                                            
                                            <div class="row">
                                                <!-- Cust id  -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Farmer id</label>
                                                        <div class="col-md-9">
                                                            <input type="number" readonly value="<?php echo e($purchaser[0]->id); ?>" class="form-control custid" id="custid" name="custid" placeholder="Farmer id">
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- Name -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Name</label>
                                                        <input type="hidden" value="<?php echo e($purchaser[0]->id); ?>" class="form-control custid" id="custid" name="custid">
                                                        <div class="col-md-9">
                                                            <input type="text" readonly class="form-control custname" id="custname" name="custname" value="<?php echo e($purchaser[0]->name); ?>" placeholder="Customer name">
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- phone number  -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Phone</label>
                                                        <div class="col-md-9">
                                                            <input type="number" readonly value="<?php echo e($purchaser[0]->phone); ?>" class="form-control phone" id="phone" name="phone" placeholder="Phone number ">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <!-- Pur id  -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Customer id</label>
                                                        <?php

                                                        ?>
                                                        <div class="col-md-9">
                                                            <input type="number" readonly value="<?php echo e($customer[0]->id); ?>" class="form-control purid" id="purid" name="purid" placeholder="Purchaser id">
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Name -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Name</label>
                                                        <input type="hidden" value="<?php echo e($customer[0]->id); ?>" class="form-control purid" id="purid" name="purid">
                                                        <div class="col-md-9">
                                                            <input type="text" readonly class="form-control purname" id="purname" name="purname" value="<?php echo e($customer[0]->name); ?>" placeholder="Customer name">
                                                        </div>
                                                    </div>
                                                </div>


                                                <!-- phone number  -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Phone</label>
                                                        <div class="col-md-9">
                                                            <input type="number" readonly value="<?php echo e($customer[0]->phone); ?>" class="form-control phone" id="phone2" name="phone2" placeholder="Phone number ">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/row-->
                                            <br>
                                            <?php
                                            $total = 0;
                                            $gran_tot = 0;
                                            foreach ($sales_bill as $sales_bil) {

                                                $total += $sales_bil->total;
                                            }
                                            $total = $total;
                                            $gran_tot = $total;
                                            ?>
                                            <hr>


                                            <h3 class="form-section">Items</h3>
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="portlet-body">
                                                        <div class="table-responsive" style="height:120px !important;">
                                                            <table class="table table-striped table-bordered table-hover">
                                                                <thead>
                                                                    <tr class="d-flex">
                                                                        <th class="text-center col-md-2"> Item </th>
                                                                        <th class="text-center  col-md-1"> Code </th>
                                                                        <th class="text-center"> Rate </th>
                                                                        <th class="text-center"> Unit </th>
                                                                        <th class="text-center"> Qty </th>
                                                                        <th class="text-center"> Total </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr id="sales-table">
                                                                        <td><input type="text" readonly id="typeahead" name="typeahead" class="form-control form-filter input-sm typeahead" />
                                                                            <input readonly type="hidden" id="prodid" name="prodid" class="form-control form-filter input-sm prodid" />
                                                                        </td>
                                                                        <td> <input  type="text"  class="form-control form-filter input-sm" id="item-code" name="item-code"> </td>
                                                                        <td> <input required type="number" min="0" readonly class="form-control form-filter input-sm" id="item-rate" name="item-rate"> </td>
                                                                        <td> <input required type="text" readonly class="form-control form-filter input-sm" id="item-unit" name="item-unit">
                                                                        </td>
                                                                        <td> <input required type="number" min="0" readonly class="form-control form-filter input-sm" id="item-qty" name="item-qty">
                                                                            <input type="hidden" class="form-control form-filter input-sm" id="item-qtyhi" name="item-qtyhi">
                                                                        </td>
                                                                        <td> <input required type="text" value="0" readonly class="form-control form-filter input-sm" id="item-totall" name="item-totall"> </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>

                                                        <div class="row">
                                                            <div class="alert alert-danger display-hide" id="alert">
                                                                <button class="close" data-close="alert"></button> You have some form errors. Please check below.
                                                            </div>
                                                            <div class="alert alert-success display-hide" id="alert2">
                                                                <button class="close" data-close="alert"></button> Your Data is successful Saved!
                                                            </div>
                                                        </div>
                                                        <center>
                                                            <div class="btn-group">
                                                                <button type="submit" id="sample_editable_1_new_sales" class="btn green"> Add
                                                                    <i class="fa fa-plus"></i>
                                                                </button>
                                                            </div>
                                                        </center>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/row-->
                                        </div>
                                        <div class="portlet-body form">
                                            <!-- BEGIN FORM-->
                                            <br>
                                            <center><p><b>വിൽപ്പന</b></p></center>
                                            <div class="portlet-body">
                                                <table class="table table-striped table-hover table-bordered " id="sample_editable_1_sales">
                                                    <thead>
                                                        <tr class="d-flex">
                                                            <th> # </th>
                                                            <th class="text-center col-md-2"> Item </th>
                                                            <th class="text-center col-md-1"> Code </th>
                                                            <th class="text-center"> Rate </th>
                                                            <th class="text-center"> Unit </th>
                                                            <th class="text-center"> Qty </th>
                                                            <th class="text-center"> Total </th>
                                                            <th class="text-center"> Delete</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php $i = 1; ?>
                                                        <?php if(count($sales_bill_temps) > 0): ?>
                                                        <?php $__currentLoopData = $sales_bill_temps; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sales_bill_temp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                                        <tr id="<?php echo e($sales_bill_temp->id); ?>">
                                                            <td> <?php echo e($i++); ?> </td>
                                                            <?php
                                                            $invoice = DB::table('products')
                                                                ->select('*')
                                                                ->where('id', $sales_bill_temp->product_id)
                                                                ->get();
                                                            ?>
                                                            <td> <?php echo e($invoice[0]->name); ?> </td>
                                                            <td> <?php echo e($invoice[0]->code); ?> </td>
                                                            <td> <?php echo e($sales_bill_temp->rate); ?> </td>
                                                            <td> <?php echo e($invoice[0]->unit); ?> </td>
                                                            <td> <?php echo e($sales_bill_temp->quantity); ?> </td>
                                                            <td> <?php echo e($sales_bill_temp->total); ?> </td>
                                                            <td>
                                                                <a class="deletesales" href="javascript:;"> Delete </a>
                                                            </td>
                                                        </tr>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                        <?php endif; ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                        <!-- //purchase -->
                                        <center><p><b>വാങ്ങൽ</b></p></center>
                                        <div class="portlet-body">
                                            <table class="table table-striped table-hover table-bordered " id="sample_editable_1_sales">
                                                <thead>
                                                    <tr class="d-flex">
                                                        <th> # </th>
                                                        <th class="text-center"> Item </th>
                                                        <th class="text-center"> Code </th>
                                                        <th class="text-center"> Rate </th>
                                                        <th class="text-center"> Unit </th>
                                                        <th class="text-center"> Qty </th>
                                                        <th class="text-center"> Gross </th>
                                                        <th class="text-center"> Commission 5%</th>
                                                        <th class="text-center"> Total </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $i = 1; ?>
                                                    <?php if(count($sales_bill_temps) > 0): ?>
                                                    <?php $__currentLoopData = $sales_bill_temps; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sales_bill_temp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                                    <tr id="<?php echo e($sales_bill_temp->id); ?>">
                                                        <td> <?php echo e($i++); ?> </td>
                                                        <?php
                                                        $invoice = DB::table('products')
                                                            ->select('*')
                                                            ->where('id', $sales_bill_temp->product_id)
                                                            ->get();
                                                        ?>
                                                        <td> <?php echo e($invoice[0]->name); ?> </td>
                                                        <td> <?php echo e($invoice[0]->code); ?> </td>
                                                        <td> <?php echo e($sales_bill_temp->rate); ?> </td>
                                                        <td> <?php echo e($invoice[0]->unit); ?> </td>
                                                        <td> <?php echo e($sales_bill_temp->quantity); ?> </td>
                                                        <td> <?php echo e($sales_bill_temp->total); ?>

                                                        <td> - <?php echo e($sales_bill_temp->com); ?> </td>
                                                        <td> <?php echo e(($sales_bill_temp->total)-($sales_bill_temp->com)); ?> </td>
                                                    </tr>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <?php endif; ?>
                                                </tbody>
                                            </table>
                                        </div>

                                        <!--  -->
                                        <center>
                                            <div class="form-actions">
                                                <button type="submit" class="btn green">Submit</button>
                                                <a href="<?php echo e(route('sales.temp.delete')); ?>" onclick="javascript:check=confirm( 'Do You Want To Cancel the Bill?'); if(check==false) return false;"><button type="button" class="btn default">Delete</button></a>
                                            </div>
                                        </center>
                                    </form>
                                    <!-- END FORM-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
                <?php else: ?>
                <div class="page-content-inner">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-gift"></i>
                                    </div>
                                    <!-- <div class="tools">
                                        <a href="javascript:;" class="collapse"> </a>
                                        <a href="javascript:;" class="reload"> </a>
                                    </div> -->
                                </div>
                                <div class="portlet-body">

                                    <div class="row">
                                        <div class="alert alert-danger display-hide" id="alert">
                                            <button class="close" data-close="alert"></button> You have some form errors. Please check below.
                                        </div>
                                        <div class="alert alert-success display-hide" id="alert2">
                                            <button class="close" data-close="alert"></button> Your Data is successful Saved!
                                        </div>
                                    </div>
                                    <!-- BEGIN FORM-->
                                    <!-- <form action="#" class="form-horizontal"> -->
                                    <div class="form-body">
                                        <h3 class="form-section">Info</h3>
                                        <div class="row">

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Date</label>
                                                    <div class="col-md-9">
                                                        <input type="date" class="form-control" id="cudate" name="cudate" required placeholder="Dadte">
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <!-- Bill number automatic -->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Bill</label>
                                                    <div class="col-md-9">
                                                        <input type="text" required class="form-control" id="billno" name="billno" readonly value="<?php echo e($innum); ?>" placeholder="Automatic">
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <hr>
                                        <br>
                                        <div class="row">
                                            <!-- farmer id -->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Farmer id</label>
                                                    <div class="col-md-9">
                                                        <input type="text" required class="form-control custid" id="custid" name="custid" placeholder="Farmer id">
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Name -->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Name</label>
                                                    <div class="col-md-9">
                                                        <input type="text" readonly class="form-control custname" id="custname" name="custname" placeholder="Farmer name">
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- phone number  -->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Phone</label>
                                                    <div class="col-md-9">
                                                        <input type="number" readonly class="form-control phone" id="phone" name="phone" placeholder="Phone number ">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <hr>
                                        <!--/row-->

                                        <div class="row">
                                            <!-- farmer id -->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Customer id</label>
                                                    <div class="col-md-9">
                                                        <input type="text" required class="form-control purid" id="purid" name="purid" placeholder="Customer ID">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Name</label>
                                                    <div class="col-md-9">
                                                        <input type="hidden" class="form-control purid" id="purid" name="purid">
                                                        <input type="text" readonly class="form-control purname" id="purname" name="purname" placeholder="Customer name">
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- phone number  -->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Phone</label>
                                                    <div class="col-md-9">
                                                        <input type="number" readonly class="form-control phone" id="phone2" name="phone2" placeholder="Phone number ">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                        <!--/row-->

                                        <h3 class="form-section">Items</h3>
                                        <!--/row-->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="portlet-body">
                                                    <div class="table-responsive" style="height:120px !important;">
                                                        <table class="table table-striped table-bordered table-hover">
                                                            <thead>
                                                                <tr class="d-flex">
                                                                    <th class="text-center col-md-2"> Item </th>
                                                                    <th class="text-center  col-md-1"> Code </th>
                                                                    <th class="text-center"> Rate </th>
                                                                    <th class="text-center"> Unit </th>
                                                                    <th class="text-center"> Qty </th>
                                                                    <th class="text-center"> Total </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr id="sales-table">
                                                                    <td><input type="text" readonly id="typeahead" name="typeahead" class="form-control form-filter input-sm typeahead" />
                                                                        <input readonly type="hidden" id="prodid" name="prodid" class="form-control form-filter input-sm prodid" />
                                                                    </td>
                                                                    <td> <input  type="text" class="form-control form-filter input-sm item-code" id="item-code" name="item-code"> </td>
                                                                    <td> <input required type="number" min="0" readonly class="form-control form-filter input-sm" id="item-rate" name="item-rate"> </td>
                                                                    <td> <input required type="text" readonly class="form-control form-filter input-sm" id="item-unit" name="item-unit">
                                                                    </td>
                                                                    <td> <input required type="number" min="0" readonly class="form-control form-filter input-sm" id="item-qty" name="item-qty">
                                                                        <input type="hidden" class="form-control form-filter input-sm" id="item-qtyhi" name="item-qtyhi">
                                                                    </td>
                                                                    <td> <input required type="text" value="0" readonly class="form-control form-filter input-sm" id="item-totall" name="item-totall"> </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>

                                                    <center>
                                                        <div class="btn-group">
                                                            <button id="sample_editable_1_new_sales" class="btn green"> Add
                                                                <i class="fa fa-plus"></i>
                                                            </button>
                                                        </div>
                                                    </center>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/row-->
                                    </div>
                                    <div class="portlet-body form">
                                        <!-- BEGIN FORM-->
                                        <div class="portlet-body">
                                            <table class="table table-striped table-hover table-bordered " id="sample_editable_1_sales">
                                                <thead>
                                                    <tr class="d-flex">
                                                        <th> # </th>
                                                        <th class="text-center col-md-2"> Item </th>
                                                        <th class="text-center col-md-1"> Code </th>
                                                        <th class="text-center"> Rate </th>
                                                        <th class="text-center"> Unit </th>
                                                        <th class="text-center"> Qty </th>
                                                        <th class="text-center"> Total </th>
                                                        <th class="text-center"> Delete</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                        <!-- END FORM-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
            <!-- END PAGE CONTENT BODY -->
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
    </div>
    <!-- END CONTAINER -->
    <?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>