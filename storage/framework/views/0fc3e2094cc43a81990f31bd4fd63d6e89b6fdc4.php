<!DOCTYPE html>
<html lang="<?php echo e(app()->getLocale()); ?>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <title>Organic Application</title>

    <link rel="shortcut icon" href="<?php echo e(asset('resources/assets/images/logo_1.png')); ?>" />

    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="<?php echo e(asset('resources/assets/global/plugins/datatables/datatables.min.css ')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('resources/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css ')); ?>" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->

    <!-- Styles -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('resources/assets/global/plugins/font-awesome/css/font-awesome.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('resources/assets/global/plugins/simple-line-icons/simple-line-icons.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('resources/assets/global/plugins/bootstrap/css/bootstrap.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('resources/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css')); ?>" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="<?php echo e(asset('resources/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('resources/assets/global/plugins/morris/morris.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('resources/assets/global/plugins/fullcalendar/fullcalendar.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('resources/assets/global/plugins/jqvmap/jqvmap/jqvmap.css')); ?>" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="<?php echo e(asset('resources/assets/global/css/components.min.css')); ?>" rel="stylesheet" id="style_components" type="text/css" />
    <link href="<?php echo e(asset('resources/assets/global/css/plugins.min.css')); ?>" rel="stylesheet" type="text/css" />
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="<?php echo e(asset('resources/assets/layouts/layout3/css/layout.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('resources/assets/layouts/layout3/css/themes/default.min.css')); ?>" rel="stylesheet" type="text/css" id="style_color" />
    <link href="<?php echo e(asset('resources/assets/layouts/layout3/css/custom.min.css')); ?>" rel="stylesheet" type="text/css" />
    <!-- END THEME LAYOUT STYLES -->
    <link href="<?php echo e(asset('resources/assets/global/plugins/typeahead/typeahead.css')); ?>" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="<?php echo e(asset('resources/assets/pages/css/jquery-ui.css')); ?>">

    <link rel="shortcut icon" href="<?php echo e(asset('public/favicon.ico')); ?>" />
    <link href="<?php echo e(asset('resources/assets/pages/css/invoice-2.min.css')); ?>" rel="stylesheet" type="text/css" />

    <link href="<?php echo e(asset('resources/assets/css/custom.css')); ?>" rel="stylesheet" type="text/css" />
    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
    <a href="<?php echo e(asset('javascript:;')); ?>" class="menu-toggler"></a>
     <!-- END RESPONSIVE MENU TOGGLER -->
     <script src="<?php echo e(asset('resources/assets/pages/scripts/form-input-mask.js')); ?>" type="text/javascript"></script>
</head>
<body class="page-container-bg-solid page-header-menu-fixed">
    <!-- BEGIN HEADER -->
    <div class="page-header" >
    <?php if(!Auth::guest()): ?>
        <!-- BEGIN HEADER TOP -->
        <div class="page-header-top">
            <div class="container">
                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                <a href="javascript:;" class="menu-toggler pull-left"></a>
                <!-- END RESPONSIVE MENU TOGGLER -->
                <!-- BEGIN LOGO -->
                <div class="page-logo">
                    <a href="<?php echo e(route('home')); ?>">
                        <img src="<?php echo e(asset('resources/assets/images/logo_3.png')); ?>" style="width: 200px;" alt="logo" class="logo-default">
                    </a>
                </div>
                <!-- END LOGO -->
                <!-- BEGIN TOP NAVIGATION MENU -->
                <div class="top-menu">
                    <ul class="nav navbar-nav pull-right">
                        <!-- BEGIN USER LOGIN DROPDOWN -->
                        <li class="dropdown dropdown-user dropdown-dark" style="">
                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                <img alt="" class="img-circle" src="<?php echo e(asset('resources/assets/layouts/layout3/img/avatar.png')); ?>">
                                <span class="username username-hide-mobile"><?php echo e(Auth::user()->name); ?></span>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-default">
                                <li><a href="<?php echo e(route('customer')); ?>"><i class="fa fa-user"></i> കസ്റ്റമർ  </a></li>
                                <!-- <li><a href="<?php echo e(route('purchaser')); ?>"><i class="fa fa-user"></i> പര്‍ച്ചേസര്‍ </a></li> -->
                                <!-- <li><a href="<?php echo e(route('backup')); ?>"><i class="fa fa-external-link-square"></i> ബാക്കപ്പ് </a></li> -->

                                <li><a href="<?php echo e(route('logout')); ?>"><i class="icon-key"></i> ലോഗ് ഔട്ട് </a></li>
                            </ul>
                        </li>
                        <!-- END USER LOGIN DROPDOWN -->
                    </ul>
                </div>
                <!-- END TOP NAVIGATION MENU -->
            </div>
        </div>
        <!-- END HEADER TOP -->
        <!-- BEGIN HEADER MENU -->
        <div class="page-header-menu">
            <div class="container">
                <!-- BEGIN MEGA MENU -->
                <!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
                <!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
                <div class="hor-menu">
                    <ul class="nav navbar-nav">
                        <li class=""><a href="<?php echo e(route('home')); ?>"> ഡാഷ്‌ബോർഡ്  </a></li>

                        <li class=" "><a href="<?php echo e(route('stock')); ?>"> ഐറ്റം  </a></li>

                        <li class=" "><a href="<?php echo e(route('sales.salesadd')); ?>"> ബിൽ </a></li>
                        
                        <li class=" "><a href="<?php echo e(route('sales.sales_edit',[0])); ?>"> എഡിറ്റുചെയ്യുക </a></li>

                        <li class="menu-dropdown classic-menu-dropdown ">
                            <a href="javascript:;"> റിപ്പോർട്ട്
                                <span class="arrow"></span>
                            </a>
                            <ul class="dropdown-menu pull-left">
                                <!-- <li class=" ">
                                    <a href="<?php echo e(route('sales.salesadd')); ?>" class="nav-link  "> ആഡ്   </a>
                                </li> -->
                                <li class=" ">
                                    <a href="<?php echo e(route('sales.salesreport')); ?>" class="nav-link  "> വിൽപ്പന  </a>
                                </li>
                                <li class=" ">
                                    <a href="<?php echo e(route('purchase.purchasereport')); ?>" class="nav-link  "> വാങ്ങൽ  </a>
                                </li>
                                <li class=" ">
                                    <a href="<?php echo e(route('customer.report')); ?>" class="nav-link  "> കസ്റ്റമർ  </a>
                                </li> 
                                <!-- <li class=" ">
                                    <a href="<?php echo e(route('sales.salesentry')); ?>" class="nav-link  "> Entry  </a>
                                </li> -->
                            </ul>
                        </li>


                        <!-- <li class=" "><a href="<?php echo e(route('expense')); ?>"> എക്സ്പെൻസ്‌ </a></li> -->

                        <li class="menu-dropdown classic-menu-dropdown ">
                            <a href="javascript:;"> പേയ്മെന്റ്
                                <span class="arrow"></span>
                            </a>
                            <ul class="dropdown-menu pull-left">
                                <li class=" ">
                                    <a href="<?php echo e(route('payment.sales')); ?>" class="nav-link  "> വിൽപ്പന  </a>
                                </li>
                                <li class=" ">
                                    <a href="<?php echo e(route('payment.purchase')); ?>" class="nav-link  "> വാങ്ങൽ  </a>
                                </li>
                                <li class=" ">
                                    <a href="<?php echo e(route('payment.customer')); ?>" class="nav-link  "> കസ്റ്റമർ  </a>
                                </li>
                                <li class=" ">
                                    <a href="<?php echo e(route('payment.purchaser')); ?>" class="nav-link  "> പര്‍ച്ചേസര്‍  </a>
                                </li>
                            </ul>
                        </li>


                        <!-- <li class=" "><a href="<?php echo e(route('dailyreport.daily')); ?>"> ഡെയ്‌ലി റിപ്പോർട്ട് </a></li> -->
                        <li class="menu-dropdown classic-menu-dropdown ">
                            <a href="javascript:;"> ഡെയ്‌ലി റിപ്പോർട്ട്
                                <span class="arrow"></span>
                            </a>
                            <ul class="dropdown-menu pull-left">
                                <li class=" ">
                                    <a href="<?php echo e(route('dailyreport.daily')); ?>" class="nav-link  "> ഡെയിലി റിപ്പോർട്ട്   </a>
                                </li>
                                <li class=" ">
                                    <a href="<?php echo e(route('dailyreport.creport')); ?>" class="nav-link  "> റിപ്പോർട്ട്  </a>
                                </li>
                                <!-- <li class=" ">
                                    <a href="<?php echo e(route('sales.salesentry')); ?>" class="nav-link  "> Entry  </a>
                                </li> -->
                            </ul>
                        </li>
                    </ul>
                </div>
                <!-- END MEGA MENU -->
            </div>
        </div>
        <!-- END HEADER MENU -->
    </div>
    <!-- END HEADER -->
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <?php echo $__env->yieldContent('content'); ?>
    </div>
    <!-- END CONTAINER -->
    <!-- BEGIN FOOTER -->
    <div class="page-footer">
        <div class="container"> Organic Application &copy; 2019.
            <a href="https://saltu.in" title="Saltu Creative Suite" target="_blank">Saltu.</a>
        </div>
    </div>
    <!-- END FOOTER -->
    <div class="scroll-to-top" id="non-printable">
        <i class="icon-arrow-up"></i>
    </div>
     <?php endif; ?>
    <!-- Scripts -->
    <script type="text/javascript" src="<?php echo e(asset('resources/assets/pages/scripts/jquery.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('resources/assets/pages/scripts/jquery.min.js')); ?>"></script>
    <script src="<?php echo e(asset('resources/assets/global/plugins/jquery.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('resources/assets/global/plugins/bootstrap/js/bootstrap.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('resources/assets/global/plugins/js.cookie.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('resources/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('resources/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('resources/assets/global/plugins/jquery.blockui.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('resources/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js')); ?>" type="text/javascript"></script>
    <!-- END CORE PLUGINS -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="<?php echo e(asset('resources/assets/global/plugins/moment.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('resources/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('resources/assets/global/plugins/morris/morris.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('resources/assets/global/plugins/morris/raphael-min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('resources/assets/global/plugins/counterup/jquery.waypoints.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('resources/assets/global/plugins/counterup/jquery.counterup.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('resources/assets/global/plugins/fullcalendar/fullcalendar.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('resources/assets/global/plugins/flot/jquery.flot.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('resources/assets/global/plugins/flot/jquery.flot.resize.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('resources/assets/global/plugins/flot/jquery.flot.categories.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('resources/assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('resources/assets/global/plugins/jquery.sparkline.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('resources/assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('resources/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('resources/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('resources/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('resources/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('resources/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('resources/assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js')); ?>" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- END CORE PLUGINS -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="<?php echo e(asset('resources/assets/global/scripts/datatable.js ')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('resources/assets/global/plugins/datatables/datatables.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('resources/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')); ?>" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->

<!-- Stock report-->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
<!--    <script src="<?php echo e(asset('resources/assets/pages/scripts/table-datatables-editable.min.js')); ?>" type="text/javascript"></script>-->

    <!-- END PAGE LEVEL SCRIPTS -->
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="<?php echo e(asset('resources/assets/global/scripts/app.min.js')); ?>" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->

<!-- Sales report -->
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="<?php echo e(asset('resources/assets/global/scripts/app.min.js')); ?>" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="<?php echo e(asset('resources/assets/pages/scripts/form-samples.min.js')); ?>" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
    <!-- BEGIN THEME LAYOUT SCRIPTS -->
    <script src="<?php echo e(asset('resources/assets/layouts/layout3/scripts/layout.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('resources/assets/layouts/layout3/scripts/demo.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('resources/assets/layouts/global/scripts/quick-sidebar.min.js')); ?>" type="text/javascript"></script>
    <!-- END THEME LAYOUT SCRIPTS -->
<!-- end sales -->
    <script src="<?php echo e(asset('resources/assets/pages/scripts/jquery.circliful.min.js')); ?>"></script>

    <script src="<?php echo e(asset('resources/assets/pages/scripts/jquery-ui.js')); ?>"></script>
     <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="<?php echo e(asset('resources/assets/pages/scripts/table-datatables-editable-stock.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('resources/assets/pages/scripts/table-datatables-editable-expense.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('resources/assets/pages/scripts/table-datatables-editable-customer.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('resources/assets/pages/scripts/table-datatables-editable-sales.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('resources/assets/pages/scripts/table-datatables-editable-purchaser.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('resources/assets/pages/scripts/table-datatables-editable-purchase.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('resources/assets/pages/scripts/table-datatables-editable-salesreport.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('resources/assets/pages/scripts/table-datatables-editable-purchasereport.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('resources/assets/pages/scripts/table-datatables-editable-payment.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('resources/assets/pages/scripts/table-datatables-editable-dailyreport.js')); ?>" type="text/javascript"></script>
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="<?php echo e(asset('resources/assets/pages/scripts/dashboard.min.js')); ?>" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
    <script src="<?php echo e(asset('resources/assets/global/plugins/typeahead/handlebars.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('resources/assets/global/plugins/typeahead/typeahead.bundle.min.js')); ?>" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->

    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="<?php echo e(asset('resources/assets/pages/scripts/components-typeahead.js')); ?>" type="text/javascript"></script>
</body>
</html>
