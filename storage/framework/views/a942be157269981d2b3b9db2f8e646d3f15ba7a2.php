<?php $__env->startSection('content'); ?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <div class="container">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>ഡാഷ്‌ബോർഡ്
                </h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
    </div>
    <!-- END PAGE HEAD-->
    <!-- BEGIN PAGE CONTENT BODY -->
    <div class="page-content">
        <div class="container">
            <!-- BEGIN PAGE BREADCRUMBS -->
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <a href="index.html">ഹോം</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>ഡാഷ്‌ബോർഡ് </span>
                </li>
            </ul>
            <!-- END PAGE BREADCRUMBS -->
            <!-- BEGIN PAGE CONTENT INNER -->
            <div class="page-content-inner">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 margin-bottom-10">
                            <div class="dashboard-stat blue">
                                <div class="visual">
                                    <i class="fa fa-briefcase fa-icon-medium"></i>
                                </div>
                                <div class="details">
                                    <?php
                                    $accounts1 = DB::table('accounts')
                                                    ->select('*')
                                                    ->where('id',2)
                                                    ->get();
                                    ?>
                                    <div class="number"> <i class="fa fa-inr" aria-hidden="true"></i> <?php echo e($accounts1[0]->amount); ?></div>
                                    <div class="desc"> സെയ്ൽസ്  </div>
                                </div>
                                <!-- <a class="more" href="javascript:;"> View more
                                    <i class="m-icon-swapright m-icon-white"></i>
                                </a> -->
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat red">
                                <div class="visual">
                                    <i class="fa fa-shopping-cart"></i>
                                </div>
                                <div class="details">
                                    <?php
                                    $accounts2 = DB::table('accounts')
                                                    ->select('*')
                                                    ->where('id',3)
                                                    ->get();
                                    ?>
                                    <div class="number"> <i class="fa fa-inr" aria-hidden="true"></i> <?php echo e($accounts2[0]->amount); ?></div>
                                    <div class="desc"> പർച്ചെയ്‌സ്</div>
                                </div>
                                <!-- <a class="more" href="javascript:;"> View more
                                    <i class="m-icon-swapright m-icon-white"></i>
                                </a> -->
                            </div>
                        </div>
<!--
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat green">
                                <div class="visual">
                                    <i class="fa fa-group fa-icon-medium"></i>
                                </div>
                                <div class="details">
                                    <?php
                                    $accounts3 = DB::table('accounts')
                                                    ->select('*')
                                                    ->where('id',4)
                                                    ->get();
                                    ?>
                                    <div class="number"> <i class="fa fa-inr" aria-hidden="true"></i> <?php echo e($accounts3[0]->amount); ?></div>
                                    <div class="desc"> എക്സ്പെൻസ്‌   </div>
                                </div>
                                 <a class="more" href="javascript:;"> View more
                                    <i class="m-icon-swapright m-icon-white"></i>
                                </a> 
                            </div>
                        </div>
-->
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat yellow" >
                                <div class="visual">
                                    <i class="fa fa-balance-scale fa-icon-medium"></i>
                                </div>
                                <div class="details">
                                    <?php
                                    $accounts4 = DB::table('accounts')
                                                    ->select('*')
                                                    ->where('id',1)
                                                    ->get();
                                    ?>
                                    <div class="number"> <i class="fa fa-inr" aria-hidden="true"></i> <?php echo e($accounts4[0]->amount); ?></div>
                                    <div class="desc"> കമ്മീഷൻ തുക  </div>
                                </div>
                               <!--  <a class="more" href="javascript:;"> View more
                                    <i class="m-icon-swapright m-icon-white"></i>
                                </a> -->
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat yellow" >
                                <div class="visual">
                                    <i class="fa fa-balance-scale fa-icon-medium"></i>
                                </div>
                                <div class="details">
                                    <?php
                                        $accounts5 = DB::table('accounts')
                                                    ->select('*')
                                                    ->where('id',5)
                                                    ->get();
                                    ?>
                                    <div class="number"> <i class="fa fa-inr" aria-hidden="true"></i>- <?php echo e($accounts5[0]->amount); ?></div>
                                    <div class="desc"> റീഫണ്ട്  തുക  </div>
                                </div>
                               <!--  <a class="more" href="javascript:;"> View more
                                    <i class="m-icon-swapright m-icon-white"></i>
                                </a> -->
                            </div>
                        </div>
                    </div>
            </div>
            <!-- END PAGE CONTENT INNER -->
        </div>
    </div>
    <!-- END PAGE CONTENT BODY -->
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>