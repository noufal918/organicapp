<?php $__env->startSection('content'); ?>
<!-- END HEADER -->
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Sales</h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE CONTENT BODY -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE BREADCRUMBS -->
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="<?php echo e(route('home')); ?>">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a>Customer</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a>Report</a>
                    </li>
                </ul>
                <!-- END PAGE BREADCRUMBS -->
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="page-content-inner">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-gift"></i>
                                    </div>
                                    <!-- <div class="tools">
                                        <a href="javascript:;" class="collapse"> </a>
                                        <a href="javascript:;" class="reload"> </a>
                                    </div> -->
                                </div>
                                <div class="portlet-body">

                                    <div class="row">
                                        <div class="alert alert-danger display-hide" id="alert">
                                            <button class="close" data-close="alert"></button> You have some form errors. Please check below.
                                        </div>
                                        <div class="alert alert-success display-hide" id="alert2">
                                            <button class="close" data-close="alert"></button> Your Data is successful Saved!
                                        </div>
                                    </div>
                                    <!-- BEGIN FORM-->
                                    <form class="form-horizontal">
                                        <?php echo e(csrf_field()); ?>

                                        <div class="form-body">
                                            <h3 class="form-section">കസ്റ്റമർ റിപ്പോർട്ട് </h3>
                                            <br>
                                            <div class="row">
                                                <!-- Name -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Customer id</label>
                                                        <div class="col-md-9">
                                                            <input type="text" required class="form-control customer_id" id="customer_id" name="customer_id" placeholder="Farmer id">
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- Bill number automatic -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Name</label>
                                                        <div class="col-md-9">
                                                            <input type="text" readonly class="form-control customer_name" id="customer_name" name="customer_name" placeholder="name">
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- phone number  -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Phone</label>
                                                        <div class="col-md-9">
                                                            <input type="number" readonly class="form-control customer_phone" id="customer_phone" name="customer_phone" placeholder="Phone number ">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <center>
                                        <div class="form-actions">
                                            <button type="submit" id="search_customer" class="btn green search_customer">Search</button>
                                        </div>
                                        </center>
                                    </form>
                                    <!-- END FORM-->
                                    <br><br>
                                    <center><p class="form-section"> വിൽപ്പന </p></center>
                                    <div class="portlet-body">
                                        <table class="table table-striped table-hover table-bordered " id="report_sales">
                                            <thead>
                                                <tr class="d-flex">
                                                    <th class="text-center"> Sl no:  </th>
                                                    <th class="text-center"> Bill no: </th>
                                                    <th class="text-center"> Date </th>
                                                    <th class="text-center"> Sold To </th>
                                                    <th class="text-center"> Total </th>
                                                    <th class="text-center">Amount Payed</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                    </tbody>
                                        </table>
                                    </div>
                                    <br><br>
                                    <center><p class="form-section"> വാങ്ങൽ  </p></center>
                                    <div class="portlet-body">
                                        <table class="table table-striped table-hover table-bordered " id="report_purchase">
                                            <thead>
                                                <tr class="d-flex">
                                                    <th class="text-center"> Sl no:  </th>
                                                    <th class="text-center"> Bill no: </th>
                                                    <th class="text-center"> Date </th>
                                                    <th class="text-center"> Purachased from </th>
                                                    <th class="text-center"> Total </th>
                                                    <th class="text-center"> Amount Payed </th>
                                                </tr>
                                            </thead>
                                            <tbody>
<!--
                                                <tr>
                                                    <td> </td>
                                                    <td> </td>
                                                    <td> </td>
                                                    <td> </td>
                                                    <td> </td>
                                                    <td> </td>
                                                </tr>
-->
                                            </tbody>
                                        </table>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT BODY -->
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>