<script src="<?php echo e(asset('resources/assets/pages/scripts/table-datatables-editable-purchasereport.js')); ?>" type="text/javascript"></script>
<table class="table table-striped table-hover table-bordered" id="sample_editable_1_purchasereport">
    <thead>
        <tr class="d-flex">
            <th> # </th>
            <th class="text-center"> Bill No </th>
            <th class="text-center"> Purchaser Name </th>
            <th class ="text-center col-md-2"> Grand Total </th>
            <th class ="text-center"> Amount Paid </th>
            <th class="text-center"> Bill Date </th>
            <th class="text-center"> Action </th>
            <!-- <th class="text-center"> Delete</th> -->
        </tr>
    </thead>
    <tbody>
       <?php $i = 1; ?>
        <?php if(count($purchase_bill_data) > 0): ?>
            <?php $__currentLoopData = $purchase_bill_data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $purchase_bill): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                    <td> <?php echo e($i++); ?> </td>
                    <td> <?php echo e($purchase_bill->bill_no); ?> </td>
                    <?php
                        $purchaser = DB::table('customers')
                                    ->select('*')
                                    ->where('id',$purchase_bill->purchaser_id)
                                    ->get();
                        $purchase_bills = DB::table('purchase_bills')
                                    ->select('*')
                                    ->where('bill_no',$purchase_bill->bill_no)
                                    ->get();
                    ?>
                    <td> <?php echo e($purchaser[0]->name); ?> </td>
                    <td> <?php echo e($purchase_bill->total); ?> </td>
                    <td> <?php echo e($purchase_bill->amountpay + $purchase_bill->amountpayl); ?> </td>
                    <td> <?php echo e($purchase_bill->bill_date); ?> </td>
                    <td> <a href="<?php echo e(route('purchase.purchasereport.details', $purchase_bill->bill_no)); ?>">View</a> </td>
                    <!-- <td> <a href="">Delete</a> </td> -->
                </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php endif; ?>
    </tbody>
</table>
