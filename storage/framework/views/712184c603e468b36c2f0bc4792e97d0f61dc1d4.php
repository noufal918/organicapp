                                            <!-- PAYMENT JS AND SALES PAYMENT CONROLLER USED-->

<?php $__env->startSection('content'); ?>
<!-- END HEADER -->
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>എഡിറ്റുചെയ്യുക</h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE CONTENT BODY -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE BREADCRUMBS -->
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="<?php echo e(route('home')); ?>">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="#">Sales</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="#">Payment</a>
                    </li>
                </ul>
                <!-- END PAGE BREADCRUMBS -->
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="page-content-inner">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-gift"></i>
                                    </div>
                                    <!-- <div class="tools">
                                        <a href="javascript:;" class="collapse"> </a>
                                        <a href="javascript:;" class="reload"> </a>
                                    </div> -->
                                </div>
                                <div class="portlet-body">
                                    
                                    <div class="row">                                    
                                        <div class="alert alert-danger display-hide" id="alert">
                                            <button class="close" data-close="alert"></button> You have some form errors. Please check below. 
                                        </div>
                                        <div class="alert alert-success display-hide" id="alert2">
                                            <button class="close" data-close="alert"></button> Your Data is successful Saved!
                                        </div>
                                    </div>
                                    <?php if($billno == 0): ?>
                                    <!-- BEGIN FORM-->
                                    <form class="form-horizontal">
                                        <?php echo e(csrf_field()); ?>

                                        <div class="form-body">
                                            <h3 class="form-section">Payment</h3>
                                            <div class="row">                                                
                                                <!-- Bill number automatic -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Bill No</label>
                                                        <div class="col-md-9">
                                                            <input type="text" class="form-control ed_billno" id="ed_billno" name="ed_billno" placeholder="Bill No">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Date</label>
                                                        <div class="col-md-9">
                                                            <input type="date" readonly class="form-control" id="cudate" name="cudate" required placeholder="Dadte">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <h3 class="form-section">Info</h3>
                                            <hr>
                                            <br>
                                            <div class="row">
                                                <!-- farmer id -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Farmer id</label>
                                                        <div class="col-md-9">
                                                            <input type="text" readonly required class="form-control purid" id="purid" name="purid" placeholder="Farmer id">
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Name -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Farmer</label>
                                                        <div class="col-md-9">
                                                            <input type="text" readonly class="form-control purname" id="purname" name="purname" placeholder="Farmer name">
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- phone number  -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Phone</label>
                                                        <div class="col-md-9">
                                                            <input type="number" readonly class="form-control purphone" id="purphone" name="purphone" placeholder="Phone">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                            <hr>
                                            <!--/row-->

                                            <div class="row">
                                                <!-- farmer id -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Customer id</label>
                                                        <div class="col-md-9">
                                                            <input type="text" readonly required class="form-control custid" id="custid" name="custid" placeholder="Customer name">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"> Name</label>
                                                        <div class="col-md-9">
                                                            <input type="text" readonly class="form-control custname" id="custname" name="custname" placeholder="Customer name">
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- phone number  -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Phone</label>
                                                        <div class="col-md-9">
                                                            <input type="number" readonly class="form-control custphone" id="custphone" name="custphone" placeholder="Phone">
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
                                            <!--/row-->                                            
                                        </div>                                        
                                        <center>
                                        <div class="form-actions">
                                            <button type="submit" id="edit_submit" class="btn green edit_submit">Show</button>
                                        </div>
                                        </center>
                                    </form>
                                    <!-- END FORM-->
                                    <?php else: ?> 
<!---------------------- ELSE CASE------------------------------->
                                    
                                    <?php
                                    
                                        $bill = DB::table('sales_bill_details')
                                                ->select('*')
                                                ->where('bill_no', $billno)
                                                ->get();
                                    
                                        $customer = DB::table('customers')
                                                ->select('name','phone')
                                                ->where('id', $bill[0]->customer_id)
                                                ->get();

                                        $purchaser = DB::table('customers')
                                                ->select('name','phone')
                                                ->where('id', $bill[0]->purchaser_id)
                                                ->get();
                                    ?>
                                    <form class="form-horizontal">
                                        <div class="form-body">
                                            <h3 class="form-section"></h3>
                                            <div class="row">                                                
                                                <!-- Bill number automatic -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Bill No</label>
                                                        <div class="col-md-9">
                                                            <input type="text" class="form-control ed_billno" id="ed_billno" name="ed_billno" value=<?php echo e($billno); ?> placeholder="Bill No">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Date</label>
                                                        <div class="col-md-9">
                                                            <input type="date" readonly class="form-control" id="cudate" name="cudate" value="<?php echo e($bill[0]->bill_date); ?>" required placeholder="Dadte">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <h3 class="form-section">Info</h3>
                                            <hr>
                                            <br>
                                            <div class="row">
                                                <!-- farmer id -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Farmer id</label>
                                                        <div class="col-md-9">
                                                            <input type="text" readonly required class="form-control purid" id="purid" name="purid" value="<?php echo e($bill[0]->purchaser_id); ?>" placeholder="Farmer id">
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Name -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Farmer</label>
                                                        <div class="col-md-9">
                                                            <input type="text" readonly class="form-control purname" id="purname" name="purname" value="<?php echo e($purchaser[0]->name); ?>" placeholder="Farmer name">
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- phone number  -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Phone</label>
                                                        <div class="col-md-9">
                                                            <input type="number" readonly class="form-control purphone" id="purphone" name="purphone" value="<?php echo e($purchaser[0]->phone); ?>"  placeholder="Phone number ">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                            <hr>
                                            <!--/row-->

                                            <div class="row">
                                                <!-- farmer id -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Customer id</label>
                                                        <div class="col-md-9">
                                                            <input type="text" readonly required class="form-control custid" id="custid" name="custid" value="<?php echo e($bill[0]->customer_id); ?>"  placeholder="Customer ID">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"> Name</label>
                                                        <div class="col-md-9">
                                                            <input type="text" readonly class="form-control custname" id="custname" name="custname" value="<?php echo e($customer[0]->name); ?>" placeholder="Customer name">
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- phone number  -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Phone</label>
                                                        <div class="col-md-9">
                                                            <input type="number" readonly class="form-control custphone" id="custphone" name="custphone" value="<?php echo e($customer[0]->phone); ?>" placeholder="Phone">
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
                                            <!--/row-->                                            
                                        </div>                                        
                                        <center>
                                        <div class="form-actions">
                                            <button type="submit" id="edit_submit" class="btn green edit_submit">Show</button>
                                        </div>
                                        </center>
                                    </form>
                                    
                                    
                                    <div class="portlet-body ">
                                        
                                        <div class="table-toolbar">
                                            <div class="row">
                                                <div class="alert alert-danger display-hide" id="alert">
                                                    <button class="close" data-close="alert"></button> You have some form errors. Please check below.
                                                </div>
                                                <div class="alert alert-success display-hide" id="alert2">
                                                    <button class="close" data-close="alert"></button> Your Data is successful Saved!
                                                </div>
                                            </div>
                                        </div>
                                        <center><p> വിൽപ്പന </p></center>
                                        <table class="table table-striped table-hover table-bordered" id="edit_sales">
                                            <thead>
                                                <tr>
                                                    <th> Sl.No </th>
                                                    <th> Item Code </th>
                                                    <th> Rate </th>
                                                    <th> Unit </th>
                                                    <th> Qty </th>
                                                    <th> Gross </th>
                                                    <th> Returned </th>
                                                    <th> Total </th>
                                                    <th> Edit </th>
                                                    <th> Cancel </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php 
                                                    $i = 1;
                                                    $products = DB::table('sales_bills')
                                                                ->select('*')
                                                                ->where('bill_no', $billno)
                                                                ->get();
                                                    $amount = DB::table('sales_bill_details')
                                                                ->select('total','amountpay')
                                                                ->where('bill_no', $billno)
                                                                ->get();
                                                ?>
                                                <?php if(count($products) > 0 ): ?>
                                                    <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                       <?php
                                                            $prod_dtls = DB::table('products')
                                                                        ->select('code','unit')
                                                                        ->where('id', $product->product_id)
                                                                        ->get();
                                                            $gross = ($product->quantity)*($product->rate);
                                                            $return = $gross - $product->total;
                                                        ?>
                                                        <tr id="<?php echo e($product->id); ?>">
                                                            <td class="text-center"> <?php echo e($i++); ?> </td>
                                                            <td class="text-center"> <?php echo e($prod_dtls[0]->code); ?> </td>
                                                            <td class="text-center"> <?php echo e($product->rate); ?> </td>
                                                            <td class="text-center"> <?php echo e($prod_dtls[0]->unit); ?> </td>
                                                            <td class="text-center"> <?php echo e($product->quantity); ?> </td>
                                                            <td class="text-center"> <?php echo e($gross); ?> </td>
                                                            <td class="text-center"> - <?php echo e($return); ?> </td>
                                                            <td class="text-center"> <?php echo e($product->total); ?> </td>
                                                            <td class="text-center"><a class="edit" href="javascript:;"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
                                                            <td></td>
                                                        </tr>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                <?php endif; ?>
                                                        <tr id="amount">
                                                            <td class="text-center">  </td>
                                                            <td class="text-center">  </td>
                                                            <td class="text-center">  </td>
                                                            <td class="text-center">  </td>
                                                            <td class="text-center">  </td>
                                                            <td class="text-center">  </td>
                                                            <td class="text-center"><b><i>Sub Total</i></b></td>
                                                            <td class="text-center"><b><?php echo e($amount[0]->total); ?></b></td>
                                                            <td class="text-center"></td>
                                                            <td></td>
                                                        </tr>
                                            </tbody>
                                        </table>
                                        
                                        
                                        <center><p> വാങ്ങൽ  </p></center>
                                        <table class="table table-striped table-hover table-bordered" id="edit_purachase">
                                            <thead>
                                                <tr>
                                                    <th> Sl.No </th>
                                                    <th> Item	Code </th>
                                                    <th> Rate </th>
                                                    <th> Unit </th>
                                                    <th> Qty </th>
                                                    <th> Gross </th>
                                                    <th> Returned </th>
                                                    <th> Commission 5% </th>
                                                    <th> Total </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php 
                                                    $i = 1;
                                                    $products = DB::table('purchase_bills')
                                                                ->select('*')
                                                                ->where('bill_no', $billno)
                                                                ->get();
                                                
                                                    $amount = DB::table('purchase_bill_details')
                                                                ->select('total','amountpay')
                                                                ->where('bill_no', $billno)
                                                                ->get();
                                                ?>
                                                <?php if(count($products) > 0 ): ?>
                                                    <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                       <?php
                                                            $prod_dtls = DB::table('products')
                                                                        ->select('code','unit')
                                                                        ->where('id', $product->product_id)
                                                                        ->get();
                                                            $gross = (($product->product_rate)*($product->quantity));
                                                            $comm = ($gross * 5)/100;
                                                            $return = ($gross - $comm) - $product->total;
                                                            
                                                        ?>
                                                        <tr id="<?php echo e($product->id); ?>">
                                                            <td class="text-center"> <?php echo e($i++); ?> </td>
                                                            <td class="text-center"> <?php echo e($prod_dtls[0]->code); ?> </td>
                                                            <td class="text-center"> <?php echo e($product->product_rate); ?> </td>
                                                            <td class="text-center"> <?php echo e($prod_dtls[0]->unit); ?>  </td>
                                                            <td class="text-center"> <?php echo e($product->quantity); ?> </td>
                                                            <td class="text-center"> <?php echo e($gross); ?> </td>
                                                            <td class="text-center"> - <?php echo e($return); ?></td>
                                                            <td class="text-center"> - <?php echo e($comm); ?></td>
                                                            <td class="text-center"> <?php echo e($product->total); ?> </td>
                                                        </tr>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                <?php endif; ?>
                                                        <tr id="amount">
                                                            <td class="text-center">  </td>
                                                            <td class="text-center">  </td>
                                                            <td class="text-center">  </td>
                                                            <td class="text-center">  </td>
                                                            <td class="text-center">  </td>
                                                            <td class="text-center">  </td> 
                                                            <td class="text-center">  </td>  
                                                            <td class="text-center"><b><i>Sub Total</i></b></td>
                                                            <td class="text-center"><b><?php echo e($amount[0]->total); ?></b></td>
                                                        </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    
                                    <center>
                                        <div class="btn-group">
                                            <button id="update_bill" class="btn green"> Return
                                                <i class="fa fa-plus"></i>
                                            </button>
                                        </div>
                                    </center>
                                    
                                    <?php endif; ?>
<!---------------------- END ELSE CASE--------------------------->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT BODY -->
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>