<?php $__env->startSection('content'); ?>
<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
    <div class="container">
        <!-- BEGIN PAGE BREADCRUMBS -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="index.html">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="#">Sales</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Print</span>
            </li>
        </ul>
    </div>
    <div style="width: 280mm; height:auto; margin: 0 auto;" >
        <table width="100%" style="border:1px solid;" cellspacing="0" cellpadding="0">
            <tr>
                <td style="height:30px; border-bottom:1px solid;" valign="top">
                    <h1 style="text-align:center; font-weight:bolder; text-transform:uppercase; margin-top:3px; margin-bottom:3px;"><b>Tax Invoice</b></h1>
                </td>
            </tr>
            <tr>
                <td style="height:60px;" valign="top">
                    <table width="100%" cellspacing="0" cellpadding="0">
                        <tr>
                                <td width="50%" style="height:60px; border-right:1px solid;">
<!--                                    <span style="position:absolute; margin-top: 30px;"><img src="<?php echo e(asset('resources/assets/images/logo_black.png')); ?>" style="width: 150px; margin-left:20px;" alt="logo"></span>-->
                                    <p style="font-size:25px; font-weight:bolder; text-align:center;">ഗ്രാമശ്രീ ഹരിത സംഘം </p>
                                                                </td>
                                    <td width="50%" style="height:60px;" valign="top">
                                    <p style="text-align:center; font-weight:bold; margin-bottom:0px; margin-top:4px;">  മാവേലിക്കര  </p>
                                    <p style="text-align:center; font-weight:bold; margin-top:4px; margin-bottom:0px;">  ആലപ്പുഴ  - 690101</p>
                                    <p style="text-align:center; font-weight:bold; margin-bottom:0px; margin-top:4px;"><i class="fa fa-phone" aria-hidden="true"></i>ഫോൺ നമ്പർ </p>
                                    <p style="text-align:center; font-weight:bold;  margin-top:4px; margin-bottom:4px;"><i class="fa fa-envelope-o" aria-hidden="true"></i> | <i class="fa fa-globe" aria-hidden="true"></i></p>
                                </td>
                            </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td width="100%">
                    <table width="100%" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="50%" style="border-top:1px solid; height:25px; border-right:1px solid; background-color:#eaeaea; text-align:center;">Details of Customer (Billed to)</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td width="100%">
                    <table width="100%" cellspacing="0" cellpadding="0">
                        <?php
                        $sales_bill_temps_va = DB::table('customers')
                        ->select('*')
                        ->where('id',$sales_bill[0]->customer_id,$sales_bill[0]->purchaser_id)
                        ->get();
                        ?>
                        <tr>
                            <td width="50%" style="border-top:1px solid; height:80px; border-right:1px solid; padding-left:40px;" valign="top">
                                <!--Billed To Details -->
                                <table >
                                    <tr>
                                        <td style="height:20px;">Invoice Number </td>
                                        <td>: <?php echo e($sales_bill[0]->bill_no); ?></td>
                                    </tr>
                                    <tr>
                                        <td style="height:20px;">Name</td>
                                        <td>: <?php echo e($sales_bill_temps_va[0]->name); ?></td>
                                    </tr>
                                </table>
                                <!--Billed To Details -->
                            </td>
                            <td width="50%" style="border-top:1px solid; padding-left:40px;" valign="top">
                                <!--Shipped To Details -->
                                <table >
                                    <tr>
                                        <td style="height:20px;">Phone Number</td>
                                        <td>: <?php echo e($sales_bill_temps_va[0]->phone); ?></td>
                                    </tr>
                                    <tr>
                                        <td style="height:20px;"> Date</td>
                                        <td>: <?php echo e($sales_bill[0]->bill_date); ?></td>
                                    </tr>
                                </table>
                                <!--Shipped To Details -->
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td width="100%">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                            <td style="border-top:1px solid; border-right:1px solid; height:20px; text-align:center;" width="3%">No</td>
                            <td width="24%" style="border-top:1px solid; border-right:1px solid; text-align:center; height:20px;"> Item </td>
                            <td width="10%" style="border-top:1px solid; border-right:1px solid; text-align:center; height:20px;"> Code </td>
                            <td width="6%" style="border-top:1px solid; border-right:1px solid; text-align:center; height:20px;">Qty</td>
                            <td width="10%" style="border-top:1px solid; border-right:1px solid; text-align:center; height:20px;">RATE</td>
                            <td width="8%" style="border-top:1px solid; border-left:1px solid; text-align:center; height:20px;">TOTAL</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <?php $i = 1; ?>
            <?php if(count($sales_bill_temps) > 0): ?>
            <?php $__currentLoopData = $sales_bill_temps; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sales_bill_temp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php
            $invoice = DB::table('products')
            ->select('*')
            ->where('id',$sales_bill_temp->product_id)
            ->get();
            ?>
            <tr>
                <td width="100%">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                            <td style="border-top:1px solid; border-right:1px solid; height:30px; text-align:center;" width="3%"><?php echo e($i++); ?></td>
                            <td width="24%" style="border-top:1px solid; border-right:1px solid; text-align:center; height:30px;"><?php echo e($invoice[0]->name); ?></td>
                            <td width="10%" style="border-top:1px solid; border-right:1px solid; text-align:center; height:30px;"><?php echo e($invoice[0]->code); ?></td>
                            <td width="6%" style="border-top:1px solid; border-right:1px solid; text-align:center; height:30px;"><?php echo e($sales_bill_temp->quantity); ?></td>
                            <td width="10%" style="border-top:1px solid; border-right:1px solid; text-align:center; height:30px;"><?php echo e($sales_bill_temp->rate); ?></td>
                            <td width="8%" style="border-top:1px solid; border-left:1px solid;  text-align:center; height:30px;"><?php echo e($sales_bill_temp->total); ?></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php endif; ?>
            <?php
            $total = 0;

            if(count($sales_bill) > 0){
            foreach($sales_bill as $sales_bill_temp){

            $total += $sales_bill_temp->total;
            }
            }
            $product_tot = $total;
            $gran_tot = 0;

            $sub_tot = $total;

            $rnd_total_am = round($sub_tot);
            $rnd_am = round(($rnd_total_am - $sub_tot),2);
            $gran_tot  = $rnd_total_am;
            $decimal=0 ;
            $decimal = round($gran_tot - ($no = floor($gran_tot)), 2) * 100;
            $hundred = null;
            $digits_length = strlen($no);
            $i = 0;
            $str = array();
            $words = array(0 => '', 1 => 'One', 2 => 'Two',
            3 => 'Three', 4 => 'Four', 5 => 'Five', 6 => 'Six',
            7 => 'seven', 8 => 'Eight', 9 => 'Nine',
            10 => 'Ten', 11 => 'Eleven', 12 => 'Twelve',
            13 => 'Thirteen', 14 => 'Fourteen', 15 => 'Fifteen',
            16 => 'Sixteen', 17 => 'Seventeen', 18 => 'Eighteen',
            19 => 'Nineteen', 20 => 'Twenty', 30 => 'Thirty',
            40 => 'Forty', 50 => 'Fifty', 60 => 'Sixty',
            70 => 'Seventy', 80 => 'Eighty', 90 => 'Ninety');
            $digits = array('', 'Hundred','Thousand','Lakh', 'Crore');
            while( $i < $digits_length ) {
            $divider = ($i == 2) ? 10 : 100;
            $number = floor($no % $divider);
            $no = floor($no / $divider);
            $i += $divider == 10 ? 1 : 2;
            if ($number) {
            $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
            $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
            $str [] = ($number < 21) ? $words[$number].' '. $digits[$counter]. $plural.' '.$hundred:$words[floor($number / 10) * 10].' '.$words[$number % 10]. ' '.$digits[$counter].$plural.' '.$hundred;
            } else $str[] = null;
            }
            $Rupees = implode('', array_reverse($str));
            $paise = ($decimal) ? "." . ($words[$decimal / 10] . " " . $words[$decimal % 10]) . ' Paise' : '';
            //                        return ($Rupees ? $Rupees . 'Rupees ' : '') . $paise .;
            $amount =  ($Rupees ? $Rupees . 'Rupees ' : '') . $paise ."" . "Only";
            ?>
            <tr>
                <td width="100%">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                            <td style="border-top:1px solid; border-right:1px solid; height:30px; text-align:center;" width="3%"></td>
                            <td width="24%" style="border-top:1px solid; border-right:1px solid; text-align:center; height:30px;"></td>
                            <td width="10%" style="border-top:1px solid; border-right:1px solid; text-align:center; height:30px;"></td>
                            <td width="6%" style="border-top:1px solid; border-right:1px solid; text-align:center; height:30px;"></td>
                            <td width="10%" style="border-top:1px solid; border-right:1px solid; text-align:center; height:30px;"></td>
                            <td width="8%" style="border-top:1px solid; border-left:1px solid;  text-align:center; height:30px;"></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td width="100%">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                            <td style="border-top:1px solid;  height:35px; text-align:center;" width="70.5%">
                            </td>
                            <td style="border-top:1px solid;  border-left:1px solid; text-align:center; height:20px; font-weight: bold;" width="16.4%">
                            TOTAL</td>
                            <td style="border-top:1px solid; border-left:1px solid;  text-align:center; height:20px; font-weight: bold;"><?php echo e($total); ?></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td width="100%">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                            <td style="border-top:1px solid;  height:35px; text-align:center;" width="70.5%">
                            </td>
                            <td style="border-top:1px solid;  border-left:1px solid; text-align:center; height:20px; font-weight: bold;" width="16.4%">
                            Grand Total</td>
                            <td style="border-top:1px solid;  border-left:1px solid; text-align:center; height:20px; font-weight: bold;">
                            <?php echo e($gran_tot); ?></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td width="100%">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                            <td style="border-top:1px solid;  height:35px; text-align:right;" width="43.2%">
                                <div>
                                    <p style="float: left; padding-left:30px;"> Total Amount in words : </p>
                                    <p style="float: right; padding-right:20px; font-weight: bold;">  <?php echo e($amount); ?> </p>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td width="100%">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                            <td style="border-top:1px solid; border-right:1px solid;  height:35px; padding-left:10px;" width="50%">
                                <p style="text-align:left:">Note:</p>
                                <p style="text-align:left:">1. Warranty for the item will be provided from the respective companies.</p>
                                <p style="text-align:left:">2. Thank you for your Business</p>
                            </td>
                            <td width="50%" style="border-top:1px solid;  text-align:center; height:20px;">
                                <p style="text-align:left:">Authorised Signatory:</p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <br>
    <div class="container" id="non-printable">
        <form method="POST" action="<?php echo e(route('sales.add')); ?>">
            <?php echo e(csrf_field()); ?>

            <div class="row" style="padding-left: 65px;">
                <div class="col-md-6">
                    <input type="radio" name="smode" checked id="smode1" value="cash" style="margin: 0 8px 0 8px;" >Cash&nbsp&nbsp&nbsp&nbsp
                    <input type="radio" name="smode" id="smode2" value="bank"  style="margin: 0 8px 0 8px;">Bank
                </div>
            </div>

            <div class="row" style="padding-top: 15px; padding-left: 65px;">
                <div class="col-md-2">
                    <input type="number" id="amountpay" name="amountpay" required class="form-control spinner" placeholder="Amount Pay">
                    <input type="hidden" class="form-control custid" id="amountpay_temp" name="amountpay_temp" value="<?php echo e($gran_tot); ?>">
                </div>
                <div class="col-md-3 prefdiv display-hide">
                    <input type="text" id="pref" name="pref" required class="form-control spinner display-hide" disabled placeholder="Payment Reference">
                </div>
                <button type="submit" class="btn green">Submit Your Invoice <i class="fa fa-check"></i></button>
                <a href="<?php echo e(route('sales.temp.delete')); ?>" onclick="javascript:check=confirm( 'Do You Want To Cancel the Bill?'); if(check==false) return false;"><button type="button" class="btn default">Delete</button></a>
                <a href="<?php echo e(route('sales.salesadd')); ?>"><button type="button" class="btn default">Edit</button></a>

                <div class="col-md-2 pull-right">
                    <a class="btn btn-lg green-haze hidden-print uppercase print-btn" onclick="window.print();">Print <i class="fa fa-print"></i></a>
                </div>
            </div>
            <div  class="row" style="padding-left: 80px;"> <p id = "sales_alert"> </p></div>
        </form>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>