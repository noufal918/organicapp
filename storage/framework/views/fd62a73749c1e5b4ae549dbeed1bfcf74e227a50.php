<script src="<?php echo e(asset('resources/assets/pages/scripts/table-datatables-editable-dailyreport.js')); ?>" type="text/javascript"></script>
<table class="table table-striped table-hover table-bordered" id="sample_editable_1_dailyreport">
    <thead>
        <tr class="d-flex">
            <th class="text-center"> Title </th>
            <th class="text-center"> Bill No </th>
            <th class="text-center"> Holder Name </th>
            <th class="text-center"> Amount Payed </th>
        </tr>
    </thead>
    <tbody>
        <?php
            $stot = 0;
            $ptot = 0;
            $etot = 0;
        ?>
        <!-- opeinig balance -->
        <?php if(count($opening) > 0): ?>
        <tr>
            <td> Opening Balance </td>
            <td colspan="2" class="text-center"> -- </td>
            <td><?php echo e($opening[0]->opening); ?></td>
        </tr>
        <?php endif; ?>
        <!-- sales -->


        <?php if(count($sales_bills) > 0): ?>
        <?php $__currentLoopData = $sales_bills; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sales_bill): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <tr>
            <td> Sales <?php echo e($sales_bill->desc); ?> </td>
            <?php
                        $sales_billss = DB::table('sales_bill_details')
                                    ->select('*')
                                    ->where('bill_no',$sales_bill->billno)
                                    ->get();
                        $customers = DB::table('customers')
                                    ->select('*')
                                    ->where('id',$sales_billss[0]->customer_id)
                                    ->get();
                    ?>
            <td> <?php echo e($sales_bill->billno); ?> </td>
            <td> <?php echo e($customers[0]->name); ?> </td>
            <td> <?php echo e($sales_bill->amount); ?> </td>
            <?php $stot += $sales_bill->amount; ?>

        </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php endif; ?>
        <!-- purchase -->
        <?php if(count($purchase_bills) > 0): ?>
        <?php $__currentLoopData = $purchase_bills; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $purchase_bill): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td> Purchase <?php echo e($purchase_bill->desc); ?> </td>
            <?php
                        $purchase_billss = DB::table('purchase_bill_details')
                                    ->select('*')
                                    ->where('bill_no',$purchase_bill->billno)
                                    ->get();
                        $customers = DB::table('customers')
                                    ->select('*')
                                    ->where('id',$purchase_billss[0]->purchaser_id)
                                    ->get();

                    ?>
            <td> <?php echo e($purchase_bill->billno); ?> </td>
            <td> <?php echo e($customers[0]->name); ?> </td>
            <td> <?php echo e($purchase_bill->amount); ?> </td>
            <?php $ptot += $purchase_bill->amount; ?>
        </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php endif; ?>
        <!-- expense -->
        <?php if(count($expenses) > 0): ?>
        <?php $__currentLoopData = $expenses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $expense): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td> Expense </td>
            <td colspan="2" class="text-center"> <?php echo e($expense->description); ?> </td>
            <td> <?php echo e($expense->amount); ?> </td>
            <?php $etot += $expense->amount; ?>
        </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php endif; ?>
        <tr>
            <td colspan="2" class="text-right">Opening Balance</td>
            <td colspan="2"><?php echo e($opening[0]->opening); ?></td>
        </tr>
        <tr>
            <td colspan="2" class="text-right">Sales Total</td>
            <td colspan="2"><?php echo e($stot); ?></td>
        </tr>
        <tr>
            <td colspan="2" class="text-right">Purchase Total</td>
            <td colspan="2"><?php echo e($ptot); ?></td>
        </tr>
        <tr>
            <td colspan="2" class="text-right">Expense Total</td>
            <td colspan="2"><?php echo e($etot); ?></td>
        </tr>
        <tr>
            <?php
            $op = $opening[0]->opening;
            $tot = ($op + $stot) - ($ptot + $etot);
            ?>
            <td colspan="2" class="text-right">Account Balance</td>
            <td colspan="2"><?php echo e($tot); ?></td>
        </tr>
    </tbody>
</table>
